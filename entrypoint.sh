#!/bin/sh
JSON_STRING='window.configs = { \
  "MDS_URL":"'"${MDS_URL}"'", \
  "US_URL":"'"${US_URL}"'", \
  "IDM_URL":"'"${IDM_URL}"'", \
  "MPS_URL":"'"${MPS_URL}"'", \
  "AUTH_URL":"'"${AUTH_URL}"'", \
  "CLIENT_ID":"'"${CLIENT_ID}"'", \
  "ACCESS_TOKEN_RENEWAL_BEFORE_EXPIRING_IN_S":"'"${ACCESS_TOKEN_RENEWAL_BEFORE_EXPIRING_IN_S}"'", \
  "DMC_STATIC_USER_ID":"'"${DMC_STATIC_USER_ID}"'", \
  "DMC_DISABLE_PREPROCESSING_CONTROLS":"'"${DMC_DISABLE_PREPROCESSING_CONTROLS}"'", \
  "DMC_STATIC_SUBFOLDER":"'"${DMC_STATIC_SUBFOLDER}"'", \
  "DMC_ORGANIZATION_NAME":"'"${DMC_ORGANIZATION_NAME}"'" \
}'
sed -i "s@// CONFIGURATIONS_PLACEHOLDER@${JSON_STRING}@" /app/index.html
exec "$@"