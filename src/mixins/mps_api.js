import axios from 'axios'
import getEnv from './env'

const mpsUrl = getEnv('MPS_URL')

const mps = {
  // Marketplace Service
  async getTags () {
    const response = await axios.get(mpsUrl + '/v1/public/tags')
    return response.data
  }
}

export default mps
