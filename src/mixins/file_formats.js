const getFolder = (file) => {
  let folder = file.fullPath.substring(0, file.fullPath.lastIndexOf('/'))
  if (folder.charAt(0) === '/') {
    folder = folder.substring(1)
  }
  return folder
}

const supportedFormats = [
  'bif',
  'dcm',
  'isyntax',
  'mrxs',
  'ndpi',
  'ome.btf',
  'ome.tf2',
  'ome.tf8',
  'ome.tif',
  'ome.tiff',
  'scn',
  'svs',
  'tif',
  'tiff',
  'vsf']

const supportedMultiFileFormats = {
  dcm: {
    // expected file structure:
    // FOLDER/PARENTFILE.dcm
    // FOLDER/LINKEDFILE.dcm
    fileExtensions: ['dcm'],
    isLinkedFile: function (parentFile, file) {
      return file.fileExtension === 'dcm' && parentFile.uid !== file.uid && getFolder(file) === getFolder(parentFile)
    },
    folderFormat: true,
    // storage structure after upload:
    // PARENT_FILE_UID/PARENT_FILE_UID.dcm
    // PARENT_FILE_UID/LINKED_FILE_UID.dcm
    getLinkedFilePath: function (file) {
      return file.parentFile.uid + '/' + file.uid + '.dcm'
    },
    getParentFilePath: function (file) {
      return file.uid + '/' + file.uid + '.dcm'
    }
  },
  mrxs: {
    // expected file structure:
    // PARENTFILE.mrxs
    // PARENTFILE/LINKEDFILE.ini
    // PARENTFILE/LINKEDFILE.dat
    fileExtensions: ['dat', 'ini'],
    isLinkedFile: function (parentFile, file) {
      var expectedLinkedFileFolder = parentFile.fileBase
      if (getFolder(parentFile)) {
        expectedLinkedFileFolder = getFolder(parentFile) + '/' + parentFile.fileBase
      }
      return getFolder(file) === expectedLinkedFileFolder
    },
    // storage structure after upload:
    // PARENT_FILE_UID.mrxs
    // PARENT_FILE_UID/LINKEDFILE.ini
    // PARENT_FILE_UID/LINKEDFILE.data
    getLinkedFilePath: function (file) {
      return file.parentFile.uid + '/' + file.file.name
    },
    getParentFilePath: function (file) {
      return file.uid + '.' + file.fileExtension
    }
  },
  vsf: {
    // expected file structure:
    // FOLDER/PARENTFILE.vsf
    // FOLDER/LINKEDFILE.img
    // FOLDER/LINKEDFILE.jpg
    fileExtensions: ['img', 'jpg'],
    isLinkedFile: function (parentFile, file) {
      return file.fileBase.includes(parentFile.fileBase) && getFolder(file) === getFolder(parentFile)
    },
    // storage structure after upload:
    // PARENT_FILE_UID/PARENT_FILE_UID.vsf
    // PARENT_FILE_UID/PARENT_FILE_UID-levelXX.img
    // PARENT_FILE_UID/PARENT_FILE_UID-slide.jpg
    getLinkedFilePath: function (file) {
      return file.parentFile.uid + '/' + file.file.name.replace(file.parentFile.fileBase, file.parentFile.uid)
    },
    getParentFilePath: function (file) {
      return file.uid + '/' + file.file.name.replace(file.fileBase, file.uid)
    }
  }
}

const supportedAnonymizationFormats = [
  'bif',
  'isyntax',
  'ndpi',
  // 'mrxs', TODO: fix problems with current version 0.4.17./ 0.4.21.
  'svs',
  'tif',
  'tiff'
]

export { supportedFormats, supportedMultiFileFormats, supportedAnonymizationFormats }
