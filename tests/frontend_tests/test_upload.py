import os
import shutil
import tempfile
import time
from io import BytesIO
from PIL import Image

import requests
from playwright.sync_api import Page

from .helpers import (
  check_console_log_contains,
  create_case,
  delete_selected_case,
  get_acces_token,
  get_data_path,
  login_as_mta,
  select_case,
  setup_console_log,
)


def get_uploaded_slides(page):
    uploaded_slide_ids = []
    for slide_card in page.locator(".slide-card").all():
        id = slide_card.get_attribute("id")
        if id and id.startswith("slide-id-"):
            uploaded_slide_ids.append(id[9:])
    return uploaded_slide_ids


def wait_for_upload_to_finish(page, already_uploaded_slides, new_slides):
    uploaded = False
    while not uploaded:
        if (len(already_uploaded_slides) + len(new_slides)) == len(
            get_uploaded_slides(page)
        ):
            uploaded = True
        time.sleep(1)


def add_files(page, files):
    page.get_by_text("add new slide").click()
    page.get_by_role("button", name="Select File").wait_for()
    with page.expect_file_chooser() as fc_info:
        page.get_by_role("button", name="Select File").click()
        file_chooser = fc_info.value
        file_chooser.set_files(files)


def upload_with_select_slide(
    page,
    new_slides,
    start_upload=True,
    wait_for_upload=True,
    pause_upload=False,
    pause_all_uploads=False,
    click_continue=False,
    extended_pause_in_seconds=0.0,
):
    add_files(page, new_slides)
    time.sleep(1)
    already_uploaded_slides = get_uploaded_slides(page)
    if start_upload:
        page.get_by_role("button", name="Click to start uploads").click()
    if click_continue:
        page.get_by_role("button", name="Continue").click()
    if pause_upload:
        # pause slide
        time.sleep(1.0)
        page.get_by_role("button").filter(has_text="pause").nth(1).click()
        page.get_by_text(f"PAUSED").nth(0).wait_for()
        time.sleep(1.0)
        page.get_by_role("button").filter(has_text="play_arrow").nth(1).click()
        page.get_by_text(f"UPLOADING", exact=True).nth(0).wait_for()
    if pause_all_uploads:
        # pause all slides
        time.sleep(1.0)
        page.get_by_role("button").filter(has_text="pause").nth(0).click()
        page.get_by_text(f"PAUSED").nth(0).wait_for()
        time.sleep(1.0)
        page.get_by_role("button").filter(has_text="play_arrow").nth(0).click()
        page.get_by_text(f"UPLOADING", exact=True).nth(0).wait_for()
    if extended_pause_in_seconds > 0.0:
        time.sleep(1.0)
        page.get_by_role("button").filter(has_text="pause").nth(0).click()
        page.get_by_text(f"PAUSED").nth(0).wait_for()
        time.sleep(extended_pause_in_seconds)
        page.get_by_role("button").filter(has_text="play_arrow").nth(0).click()
        page.get_by_text(f"UPLOADING", exact=True).nth(0).wait_for()
    if wait_for_upload:
        wait_for_upload_to_finish(page, already_uploaded_slides, new_slides)
    return list(set(get_uploaded_slides(page)) - set(already_uploaded_slides))


def validate_slide(case_id, slide_id, access_token):
    r = requests.get(
        f"http://localhost:10006/v3/cases/{case_id}?with_slides=true",
        headers={"Authorization": access_token},
    )
    ids = [slide["id"] for slide in r.json()["slides"]]
    assert slide_id in ids
    r = requests.get(
        f"http://localhost:10006/v3/slides/{slide_id}/info",
        headers={"Authorization": access_token},
    )
    r.raise_for_status()
    assert r.json()["id"] == slide_id


def validate_tile(slide_id, test_tile, access_token):
    level = test_tile["level"]
    tile_x = test_tile["tile_location"][0]
    tile_y = test_tile["tile_location"][1]
    r = requests.get(
        f"http://localhost:10006/v3/slides/{slide_id}/tile/level/{level}/tile/{tile_x}/{tile_y}",
        headers={"Authorization": access_token},
    )
    assert r.status_code == 200
    image_bytes = BytesIO(r.content)
    image = Image.open(image_bytes)
    pixel_actual = image.getpixel(tuple(test_tile["pixel_location"]))
    assert pixel_actual == tuple(test_tile["pixel_expected"])
    assert test_tile["tile_size"] == image.size[0] == image.size[1]


def validate_region(slide_id, test_region, access_token):
    level = test_region["level"]
    size_x = size_y = test_region["region_size"]
    start_x = test_region["region_location"][0]
    start_y = test_region["region_location"][1]
    r = requests.get(
        f"http://localhost:10006/v3/slides/{slide_id}/region/level/{level}/start/{start_x}/{start_y}/size/{size_x}/{size_y}",
        headers={"Authorization": access_token}
    )
    assert r.status_code == 200
    image_bytes = BytesIO(r.content)
    image = Image.open(image_bytes)
    pixel_actual = image.getpixel(tuple(test_region["pixel_location"]))
    assert pixel_actual == tuple(test_region["pixel_expected"])
    assert test_region["region_size"] == image.size[0] == image.size[1]


def validate_format(slide_id, expected_format, access_token):
    r = requests.get(
        f"http://localhost:10006/v3/slides/{slide_id}/info",
        headers={"Authorization": access_token},
    )
    r.raise_for_status()
    assert r.json()["format"] == expected_format


def validate_slide_tissue_stain(
    slide_id, access_token, expected_tissue, expected_stain
):
    r = requests.get(
        f"http://localhost:10006/v3/slides/{slide_id}",
        headers={"Authorization": access_token},
    )
    r.raise_for_status()
    assert r.json()["tissue"] == expected_tissue
    assert r.json()["stain"] == expected_stain


def validate_slide_deleted(case_id, slide_id, access_token):
    r = requests.get(
        f"http://localhost:10006/v3/cases/{case_id}?with_slides=true",
        headers={"Authorization": access_token},
    )
    slide = None
    for slide in r.json()["slides"]:
        if slide["id"] == slide_id:
            break
    assert slide["id"] == slide_id
    assert slide["deleted"]


def validate_case_deleted(case_id, access_token):
    r = requests.get(
        f"http://localhost:10006/v3/cases/{case_id}",
        headers={"Authorization": access_token},
    )
    assert r.json()["deleted"]


def validate_white_image(request):
    image_bytes = BytesIO(request.content)
    image = Image.open(image_bytes)
    pixel_actual = image.getpixel((0, 0))
    assert pixel_actual == 255


def validate_anonymization(slide_id, image_type, expected, console_log_path, access_token):
    r = requests.get(
        f"http://localhost:10006/v3/slides/{slide_id}/{image_type}/max_size/100/100",
        headers={"Authorization": access_token},
    )
    # TODO: should only be 404 not found, check with wsi service
    assert r.status_code == 200 if expected else r.status_code == 404 or r.status_code == 500
    if expected and r.status_code == 200:
        validate_white_image(r)
    check_console_log_contains(console_log_path, "Automatic anonymization successful")


def test_upload_slide_extended(page: Page):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload and validate
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    uploaded_slide_ids = upload_with_select_slide(page, files)
    slide_id = uploaded_slide_ids[0]
    access_token = get_acces_token(page)
    validate_slide(case_id, slide_id, access_token)
    validate_anonymization(slide_id, "macro", False, console_log_path, access_token)
    # try to upload the same file again
    uploaded_slide_ids = upload_with_select_slide(
        page, files, start_upload=False, wait_for_upload=False
    )
    page.get_by_text("already added").wait_for()
    # reload page, try again
    page.reload()
    time.sleep(1)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    uploaded_slide_ids = upload_with_select_slide(
        page, files, start_upload=False, wait_for_upload=False
    )
    page.set_default_timeout(60000)
    page.get_by_text("already added").wait_for()
    # delete and validate
    page.get_by_role("button").filter(has_text="delete").nth(1).click()
    page.get_by_role("button", name="Confirm").click()
    page.get_by_text(f"Slide CMU-1.svs deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    # re-upload and validate
    uploaded_slide_ids = upload_with_select_slide(page, files)
    slide_id = uploaded_slide_ids[0]
    access_token = get_acces_token(page)
    validate_slide(case_id, slide_id, access_token)
    validate_anonymization(slide_id, "macro", False, console_log_path, access_token)
    # set tissue and stain
    page.get_by_role("combobox", name="Tissue").click()
    page.get_by_role("option", name="Breast").click()
    page.get_by_text(f"Slide CMU-1.svs updated").wait_for()
    page.get_by_role("combobox", name="Stain").click()
    page.get_by_role("option", name="Actin").click()
    page.get_by_text(f"Slide CMU-1.svs updated").wait_for()
    time.sleep(1)
    validate_slide_tissue_stain(slide_id, access_token, "BREAST", "ACTIN")
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)


def test_upload_slide_pause(page: Page):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload and validate
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    uploaded_slide_ids = upload_with_select_slide(page, files, pause_upload=True)
    slide_id = uploaded_slide_ids[0]
    access_token = get_acces_token(page)
    validate_slide(case_id, slide_id, access_token)
    validate_anonymization(slide_id, "macro", False, console_log_path, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)


def test_upload_slide_resume_after_reload(page: Page):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload and reload page while uploading
    # check if resuming after reload
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    upload_with_select_slide(page, files, wait_for_upload=False)
    time.sleep(1)
    page.reload()
    time.sleep(1)
    uploaded_slide_ids = upload_with_select_slide(page, files)
    check_console_log_contains(
        console_log_path, "Resuming from previous upload with file id"
    )
    # validate
    slide_id = uploaded_slide_ids[0]
    access_token = get_acces_token(page)
    validate_slide(case_id, slide_id, access_token)
    validate_anonymization(slide_id, "macro", False, console_log_path, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)


def test_upload_slide_resume_network_offline(page: Page, context):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # disconnect network while uploading
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    upload_with_select_slide(page, files, wait_for_upload=False)
    time.sleep(1)
    context.set_offline(True)
    time.sleep(2)
    context.set_offline(False)
    wait_for_upload_to_finish(page, [], files)
    # validate
    slide_id = get_uploaded_slides(page)[0]
    access_token = get_acces_token(page)
    validate_slide(case_id, slide_id, access_token)
    validate_anonymization(slide_id, "macro", False, console_log_path, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)


def test_upload_slide_resume_network_offline_failed(page: Page, context):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # disconnect network while uploading
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    upload_with_select_slide(page, files, wait_for_upload=False)
    time.sleep(1)
    context.set_offline(True)
    time.sleep(11)
    page.get_by_role("button").filter(has_text="refresh").click()
    context.set_offline(False)
    page.get_by_role("button").filter(has_text="refresh").click()
    wait_for_upload_to_finish(page, [], files)
    # validate
    slide_id = get_uploaded_slides(page)[0]
    access_token = get_acces_token(page)
    validate_slide(case_id, slide_id, access_token)
    validate_anonymization(slide_id, "macro", False, console_log_path, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)


def test_upload_slide_resume_network_offline_failed_resume_button(page: Page, context):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # disconnect network while uploading
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    upload_with_select_slide(page, files, wait_for_upload=False)
    time.sleep(1)
    context.set_offline(True)
    page.get_by_text(
        "Connection issues. Please check your internet connection and resume failed uploads"
    ).wait_for()
    page.get_by_role("button").filter(has_text="Resume").first.click()
    context.set_offline(False)
    page.get_by_role("button").filter(has_text="Resume").first.click()
    wait_for_upload_to_finish(page, [], files)
    # validate
    slide_id = get_uploaded_slides(page)[0]
    access_token = get_acces_token(page)
    validate_slide(case_id, slide_id, access_token)
    validate_anonymization(slide_id, "macro", False, console_log_path, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)


def test_upload_broken_file(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    files = [os.path.join(get_data_path(), "empty.tif")]
    uploaded_slide_ids = upload_with_select_slide(page, files, click_continue=True)
    slide_id = uploaded_slide_ids[0]
    access_token = get_acces_token(page)
    # check error
    r = requests.get(
        f"http://localhost:10006/v3/slides/{slide_id}/info",
        headers={"Authorization": access_token},
    )
    assert r.status_code == 404
    assert "Slide format is not supported by any plugin" in r.json()["detail"]
    page.get_by_text("warning").wait_for()
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)


def test_upload_multiple_slides_pause(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload with pause and validate
    files = [
        os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs"),
        os.path.join(os.environ["TEST_DATA_DIR"], "Generic TIFF", "CMU-1.tiff"),
    ]
    uploaded_slide_ids = upload_with_select_slide(
        page, files, pause_all_uploads=True, click_continue=True
    )
    access_token = get_acces_token(page)
    for slide_id in uploaded_slide_ids:
        validate_slide(case_id, slide_id, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_case_deleted(case_id, access_token)


def test_upload_multiple_slides_remove_failed(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload with pause and validate
    files = [
        os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs"),
        os.path.join(os.environ["TEST_DATA_DIR"], "Generic TIFF", "CMU-1.tiff"),
    ]
    upload_with_select_slide(page, files, wait_for_upload=False)
    time.sleep(1)
    page.get_by_role("button").filter(has_text="Remove").click()
    page.get_by_role("button", name="Click to start uploads").click()
    wait_for_upload_to_finish(page, [], [files[0]])
    time.sleep(1)
    assert "CMU-1.tiff" not in page.content()
    access_token = get_acces_token(page)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_case_deleted(case_id, access_token)


def test_upload_multiple_slides_cancel(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload and cancel
    files = [
        os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs"),
        os.path.join(os.environ["TEST_DATA_DIR"], "Generic TIFF", "CMU-1.tiff"),
    ]
    uploaded_slide_ids = upload_with_select_slide(
        page, files, wait_for_upload=False, click_continue=True
    )
    time.sleep(1)
    page.get_by_role("button").filter(has_text="cancel").nth(0).click()
    time.sleep(1)
    # upload and validate
    uploaded_slide_ids = upload_with_select_slide(page, files)
    access_token = get_acces_token(page)
    for slide_id in uploaded_slide_ids:
        validate_slide(case_id, slide_id, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_case_deleted(case_id, access_token)


def test_upload_multiple_slides_cancel_one_slide(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload and validate
    files = [
        os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs"),
        os.path.join(os.environ["TEST_DATA_DIR"], "Generic TIFF", "CMU-1.tiff"),
    ]
    uploaded_slide_ids = upload_with_select_slide(
        page, files, wait_for_upload=False, click_continue=True
    )
    time.sleep(1)
    page.get_by_role("button").filter(has_text="cancel").nth(1).click()
    wait_for_upload_to_finish(page, [], [files[1]])
    uploaded_slide_ids = upload_with_select_slide(page, [files[0]])
    access_token = get_acces_token(page)
    for slide_id in uploaded_slide_ids:
        validate_slide(case_id, slide_id, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_case_deleted(case_id, access_token)


def test_upload_slide_test_queue_adding_file_by_file(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # add files to queue file by file and check
    files_1 = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    add_files(page, files_1)
    files_2 = [os.path.join(os.environ["TEST_DATA_DIR"], "Generic TIFF", "CMU-1.tiff")]
    add_files(page, files_2)
    page.get_by_text(f"CMU-1.svs").wait_for()
    page.get_by_text(f"CMU-1.tiff").wait_for()
    # upload and validate
    page.get_by_role("button", name="Click to start uploads").click()
    page.get_by_role("button", name="Continue").click()
    already_uploaded_slides = get_uploaded_slides(page)
    wait_for_upload_to_finish(page, already_uploaded_slides, files_1 + files_2)
    new_slides = list(set(get_uploaded_slides(page)) - set(already_uploaded_slides))
    assert len(new_slides) == 2
    access_token = get_acces_token(page)
    validate_slide(case_id, new_slides[0], access_token)
    validate_slide(case_id, new_slides[1], access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_case_deleted(case_id, access_token)


def test_upload_slide_add_while_uploading(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # add first file
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs"), os.path.join(os.environ["TEST_DATA_DIR"], "Generic TIFF", "CMU-1.tiff")]
    add_files(page, files[0])
    page.get_by_text(f"CMU-1.svs").wait_for()
    page.get_by_role("button", name="Click to start uploads").click()
    time.sleep(1)
    # add second file while uploading
    add_files(page, files[1])
    page.get_by_text(f"CMU-1.tiff").wait_for()
    page.get_by_role("button", name="Click to start uploads").click()
    page.get_by_role("button", name="Continue").click()
    time.sleep(15)
    uploaded_slide_ids = get_uploaded_slides(page)
    assert len(uploaded_slide_ids) == 2
    access_token = get_acces_token(page)
    validate_slide(case_id, uploaded_slide_ids[0], access_token)
    validate_slide(case_id, uploaded_slide_ids[1], access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_case_deleted(case_id, access_token)


def test_upload_many_small_files(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # small file used for upload
    tmpPath = tempfile.mkdtemp()
    small_slide_path = os.path.join(get_data_path(), "CMU-1-small.tiff")
    files = []
    # upload
    for i in range(50):
        shutil.copy(small_slide_path, os.path.join(tmpPath, f"{i}.tiff"))
        small_file = os.path.join(tmpPath, f"{i}.tiff")
        add_files(page, small_file)
        page.get_by_text(f"{i}.tiff").wait_for()
        page.get_by_role("button", name="Click to start uploads").click()
        time.sleep(1)
        page.get_by_role("button", name="Continue").click()
    # validate
    time.sleep(15)
    uploaded_slide_ids = get_uploaded_slides(page)
    assert len(uploaded_slide_ids) == 50
    access_token = get_acces_token(page)
    for slide_id in uploaded_slide_ids:
        validate_slide(case_id, slide_id, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    for slide_id in uploaded_slide_ids:
        validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)
    # remove files
    shutil.rmtree(tmpPath)


def test_upload_slides_to_different_cases(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name_1 = create_case(page)
    select_case(page, case_name_1)
    case_id_1 = page.url.split("/")[-1]
    # add file to first case
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    new_slides_1 = upload_with_select_slide(page, files)
    # create new case and add file to the second case
    case_name_2 = create_case(page)
    select_case(page, case_name_2)
    case_id_2 = page.url.split("/")[-1]
    new_slides_2 = upload_with_select_slide(page, files)
    # check upload to first case
    select_case(page, case_name_1)
    assert len(new_slides_1) == 1
    access_token = get_acces_token(page)
    validate_slide(case_id_1, new_slides_1[0], access_token)
    # check upload to second case
    select_case(page, case_name_2)
    assert len(new_slides_2) == 1
    access_token = get_acces_token(page)
    validate_slide(case_id_2, new_slides_2[0], access_token)
    # delete cases
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name_2} deleted").wait_for()
    validate_case_deleted(case_id_2, access_token)
    select_case(page, case_name_1)
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name_1} deleted").wait_for()
    validate_case_deleted(case_id_1, access_token)
