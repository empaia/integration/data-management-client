
const routes = [
  {
    path: '/',
    component: () => import('src/pages/Main.vue'),
    children: [
      { name: 'home', path: '', component: () => import('components/Cases.vue') },
      {
        path: 'cases',
        component: () => import('components/Cases.vue')
      },
      {
        path: 'cases/:caseId',
        props: true,
        component: () => import('components/Cases.vue')
      }
    ]

  },
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
