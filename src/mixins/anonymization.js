import AnonymizedStream from '@empaia/wsi-anon'
import { supportedAnonymizationFormats } from './file_formats'
import { getFileNameMetadata } from './uploads'

const CHUNK_SIZE = 10 * 1000 * 1000

async function anonymizeFile (file, fileGroup) {
  if (isSupportedAnonymizationFormat(file)) {
    const stream = AnonymizedStream.create(file.file, CHUNK_SIZE, getFileNameMetadata(file, fileGroup))
    for (const linkedFile of file.linkedFiles) {
      AnonymizedStream.create(linkedFile.file, CHUNK_SIZE, getFileNameMetadata(linkedFile, fileGroup))
    }
    try {
      await stream.anonymize()
      console.log(`Automatic anonymization successful for ${file.file.name}`)
      file.anonymizationSuccessful = true
      for (const linkedFile of file.linkedFiles) {
        linkedFile.anonymizationSuccessful = true
      }
      return 'anonymization successful'
    } catch (error) {
      console.log(`Automatic anonymization failed for ${file.file.name}`)
      return 'anonymization failed'
    }
  } else {
    console.log(`Automatic anonymization unsupported for ${file.file.name}`)
    return 'anonymization unsupported'
  }
}

function retrieveAnonymizedStream (file, fileGroup) {
  if (file.anonymizationSuccessful) {
    return AnonymizedStream.retrieve(getFileNameMetadata(file, fileGroup))
  } else {
    return file.file
  }
}

function destroyAnonymizedStream (file, fileGroup) {
  if (isSupportedAnonymizationFormat(file)) {
    AnonymizedStream.destroy(getFileNameMetadata(file, fileGroup))
  }
}

function isSupportedAnonymizationFormat (file) {
  return supportedAnonymizationFormats.includes(file.fileExtension) || supportedAnonymizationFormats.includes(getParentFileExtension(file))
}

function getParentFileExtension (file) {
  var parentFileExtension = null
  if (file.parentFile) {
    parentFileExtension = file.parentFile.fileExtension
  }
  return parentFileExtension
}

export { anonymizeFile, CHUNK_SIZE, destroyAnonymizedStream, retrieveAnonymizedStream }
