export default function getEnv (name) {
  const variables = {}
  variables.MDS_URL = process.env.MDS_URL
  variables.US_URL = process.env.US_URL
  variables.IDM_URL = process.env.IDM_URL
  variables.MPS_URL = process.env.MPS_URL
  variables.AUTH_URL = process.env.AUTH_URL
  variables.CLIENT_ID = process.env.CLIENT_ID
  variables.ACCESS_TOKEN_RENEWAL_BEFORE_EXPIRING_IN_S = process.env.ACCESS_TOKEN_RENEWAL_BEFORE_EXPIRING_IN_S
  variables.DMC_STATIC_USER_ID = process.env.DMC_STATIC_USER_ID
  variables.DMC_STATIC_SUBFOLDER = process.env.DMC_STATIC_SUBFOLDER
  variables.DMC_DISABLE_PREPROCESSING_CONTROLS = process.env.DMC_DISABLE_PREPROCESSING_CONTROLS
  variables.DMC_ORGANIZATION_NAME = process.env.DMC_ORGANIZATION_NAME
  return window?.configs?.[name] || variables[name]
}
