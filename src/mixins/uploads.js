import * as tus from 'tus-js-client'
import { throttle } from 'quasar'
import { anonymizeFile, CHUNK_SIZE, destroyAnonymizedStream, retrieveAnonymizedStream } from './anonymization'
import getEnv from './env'
import { supportedMultiFileFormats } from './file_formats'
import us from './us_api'
import { sleep } from './helpers'

class UploadManager {
  constructor (maxParallelUploads = 1) {
    this.maxParallelUploads = maxParallelUploads
    this.uploads = []
    this.uploadedCallback = null
    this.connectionIssueCallback = null
  }

  setUploadFinishedCallback (uploadedCallback) {
    const uploadedCallbackExtended = function (upload) {
      console.log('Finished upload of', upload.file.file.name)
      this._startWaitingUploads()
      uploadedCallback(upload)
    }.bind(this)
    this.uploadedCallback = uploadedCallbackExtended
  }

  setConnectionIssueCallback (connectionIssueCallback) {
    this.connectionIssueCallback = connectionIssueCallback
  }

  add (file, fileGroup) {
    const upload = new MultiFileUpload(file, fileGroup, this.uploadedCallback, this.connectionIssueCallback)
    this.uploads.push(upload)
    return upload
  }

  remove (multiFileUpload) {
    this.uploads = this.uploads.filter(upload => upload.file.uid !== multiFileUpload.file.uid)
    this._startWaitingUploads()
  }

  start () {
    for (const upload of this.getUploadsWithState('queued')) {
      upload.setStatus('waiting')
    }
    this._startWaitingUploads()
  }

  startUpload (upload) {
    upload.setStatus('waiting')
    this._startWaitingUploads()
  }

  pause () {
    for (const upload of this.getUploadsWithState('uploading')) {
      upload.pause()
    }
  }

  resume () {
    for (const upload of this.getUploadsWithStates(['paused', 'failed'])) {
      upload.resume()
    }
  }

  cancel () {
    for (const upload of this.getUploadsWithStates(['uploading', 'paused'])) {
      upload.cancel()
      this.remove(upload)
    }
  }

  getUploadsWithState (state) {
    return this.uploads.filter(upload => upload.getStatus() === state)
  }

  getUploadsWithStates (states) {
    return this.uploads.filter(upload => states.includes(upload.getStatus()))
  }

  isUploading () {
    return this.getUploadsWithState('uploading').length > 0
  }

  getQueuedFileCount () {
    return this.getUploadsWithState('queued').length
  }

  getCheckingFileCount () {
    return this.getUploadsWithState('checking').length
  }

  getUploadingFileCount () {
    return this.getUploadsWithState('uploading').length
  }

  async waitForAnonymization () {
    for (const upload of this.getUploadsWithState('checking')) {
      const anonymizationStatus = upload.getAnonymizationStatus()
      if (anonymizationStatus.includes('anonymizing')) {
        await sleep(1000)
        await this.waitForAnonymization()
      }
    }
  }

  hasFailedOrUnsupportedAnonymizationInQueue () {
    for (const upload of this.getUploadsWithState('queued')) {
      const anonymizationStatus = upload.getAnonymizationStatus()
      if (anonymizationStatus.includes('failed') || anonymizationStatus.includes('unsupported')) {
        return true
      }
    }
    return false
  }

  removeFailedOrUnsupportedAnonymizationInQueue () {
    for (const upload of this.getUploadsWithState('queued')) {
      const anonymizationStatus = upload.getAnonymizationStatus()
      if (anonymizationStatus.includes('failed') || anonymizationStatus.includes('unsupported')) {
        this.remove(upload)
      }
    }
  }

  getAllFileIds () {
    const fileIds = []
    for (const upload of this.uploads) {
      fileIds.push(upload.file.uid)
    }
    return fileIds
  }

  getProgress (ignoreQueued = true) {
    let totalBytes = 0
    if (ignoreQueued) {
      totalBytes += this.getTotalBytesUploading()
    } else {
      totalBytes += this.getTotalBytes()
    }
    return this.getUploadedBytes() / totalBytes
  }

  getUploadedBytes () {
    let uploadedBytes = 0
    for (const upload of this.uploads) {
      uploadedBytes += upload.getUploadedBytes()
    }
    return uploadedBytes
  }

  getTotalBytes () {
    let totalBytes = 0
    for (const upload of this.uploads) {
      totalBytes += upload.getTotalBytes()
    }
    return totalBytes
  }

  getTotalBytesQueued () {
    let totalBytes = 0
    for (const upload of this.getUploadsWithState('queued')) {
      totalBytes += upload.getTotalBytes()
    }
    return totalBytes
  }

  getTotalBytesUploading () {
    let totalBytes = 0
    for (const upload of this.getUploadsWithStates(['waiting', 'uploading', 'paused', 'uploaded', 'failed'])) {
      totalBytes += upload.getTotalBytes()
    }
    return totalBytes
  }

  _startWaitingUploads () {
    let activeUploadsCount = this.getUploadsWithState('uploading').length
    for (const waitingUpload of this.getUploadsWithState('waiting')) {
      if (activeUploadsCount < this.maxParallelUploads) {
        waitingUpload.start()
        activeUploadsCount++
      }
    }
  }
}

class MultiFileUpload {
  constructor (file, fileGroup, uploadedCallback, connectionIssueCallback) {
    this.file = file
    this.fileGroup = fileGroup
    const singleFileUploadedCallback = async function () {
      if (this.getStatus() === 'uploaded') {
        if (uploadedCallback) {
          await uploadedCallback(this)
        }
      }
    }.bind(this)
    this.uploads = []
    this.uploads.push(new Upload(file, fileGroup, singleFileUploadedCallback, connectionIssueCallback))
    for (const linkedFile of file.linkedFiles) {
      this.uploads.push(new Upload(linkedFile, fileGroup, singleFileUploadedCallback, connectionIssueCallback))
    }
    this.anonymizationStatus = 'checking'
  }

  start () {
    console.log('Started upload of', this.file.file.name)
    for (const upload of this.getUploadsWithStates(['queued', 'waiting'])) {
      upload.start()
    }
  }

  pause () {
    for (const upload of this.getUploadsWithStates(['uploading', 'waiting'])) {
      upload.pause()
    }
  }

  resume () {
    for (const upload of this.getUploadsWithStates(['paused', 'failed'])) {
      upload.resume()
    }
  }

  cancel () {
    for (const upload of this.getUploadsWithStates(['uploading', 'waiting', 'paused', 'uploaded'])) {
      upload.cancel()
    }
  }

  async anonymize () {
    this.anonymizationStatus = 'anonymizing'
    this.anonymizationStatus = await anonymizeFile(this.file, this.fileGroup)
    this.setStatus('queued')
  }

  getAnonymizationStatus () {
    // Status Overview
    // checking -> anonymizing -> anonymization successful or anonymization failed or anonymization unsupported
    return this.anonymizationStatus
  }

  setStatus (status) {
    for (const upload of this.uploads) {
      upload.setStatus(status)
    }
  }

  getStatus () {
    // Status Overview
    // checking -> queued -> waiting -> uploading or paused -> failed or uploaded
    const uploadStatusList = []
    for (const upload of this.uploads) {
      uploadStatusList.push(upload.getStatus())
    }
    if (uploadStatusList.includes('failed')) {
      return 'failed'
    }
    if (uploadStatusList.includes('uploading')) {
      return 'uploading'
    }
    if (uploadStatusList.includes('paused')) {
      return 'paused'
    }
    if (uploadStatusList.includes('waiting')) {
      return 'waiting'
    }
    if (uploadStatusList.includes('queued')) {
      return 'queued'
    }
    if (uploadStatusList.includes('checking')) {
      return 'checking'
    }
    return 'uploaded'
  }

  getUploadsWithState (state) {
    return this.uploads.filter(upload => upload.getStatus() === state)
  }

  getUploadsWithStates (states) {
    return this.uploads.filter(upload => states.includes(upload.getStatus()))
  }

  isUploading () {
    return this.getUploadsWithStates(['uploading', 'waiting']).length > 0
  }

  getProgress () {
    return this.getUploadedBytes() / this.getTotalBytes()
  }

  getUploadedBytes () {
    let uploadedBytes = 0
    for (const upload of this.uploads) {
      uploadedBytes = uploadedBytes + upload.getUploadedBytes()
    }
    return uploadedBytes
  }

  getTotalBytes () {
    let totalBytes = 0
    for (const upload of this.uploads) {
      totalBytes = totalBytes + upload.getTotalBytes()
    }
    return totalBytes
  }
}

class Upload {
  constructor (file, fileGroup, singleFileUploadedCallback, connectionIssueCallback) {
    this.file = file
    this.fileGroup = fileGroup
    this.singleFileUploadedCallback = singleFileUploadedCallback
    this.connectionIssueCallback = connectionIssueCallback
    this.status = 'checking'
    this.totalBytes = file.file.size
    this.uploadedBytes = 0
    this.tusUpload = null
    this.metadata = {
      file_name: getFileNameMetadata(this.file, this.fileGroup),
      id: this.file.uid
    }
  }

  async start () {
    this.tusUpload = this._createTusUpload()
    const previousUploads = await this.tusUpload.findPreviousUploads()
    if (previousUploads.length) {
      console.log('Resuming from previous upload with file id ' + previousUploads[0].metadata.id)
      this.tusUpload.resumeFromPreviousUpload(previousUploads[0])
    }
    this.tusUpload.start()
    this.status = 'uploading'
  }

  pause () {
    if (this.tusUpload) {
      this.tusUpload.abort()
    }
    this.status = 'paused'
  }

  resume () {
    if (this.tusUpload) {
      this.tusUpload.start()
      this.status = 'uploading'
    } else {
      this.start()
    }
  }

  cancel (deleteFromServer = true) {
    if (this.tusUpload) {
      this.tusUpload.abort()
    }
    destroyAnonymizedStream(this.file, this.fileGroup)
    this._deleteTusEntry(this.file.uid)
    if (deleteFromServer) {
      this._deleteFileFromServer(this.file.uid)
    }
  }

  getStatus () {
    return this.status
  }

  setStatus (status) {
    this.status = status
  }

  getUploadedBytes () {
    return this.uploadedBytes
  }

  getTotalBytes () {
    return this.totalBytes
  }

  getProgress () {
    return this.uploadedBytes / this.totalBytes
  }

  _createTusUpload () {
    const stream = retrieveAnonymizedStream(this.file, this.fileGroup)
    return new tus.Upload(stream, {
      endpoint: getEnv('US_URL') + `/v1/files?fileGroup=${this.fileGroup}`,
      retryDelays: [0, 1000, 2000, 3000, 4000],
      chunkSize: CHUNK_SIZE,
      uploadSize: this.file.file.size,
      parallelUploads: 1,
      metadata: this.metadata,

      onShouldRetry: function () {
        return true
      },

      onBeforeRequest: function (req) {
        let accessToken = ''
        const clientId = getEnv('CLIENT_ID')
        Object.keys(sessionStorage).forEach(function (key) {
          if (key.includes(clientId)) {
            const values = JSON.parse(sessionStorage.getItem(key))
            if ('access_token' in values) {
              accessToken = values.access_token
            }
          }
        })
        req.setHeader('Authorization', 'Bearer ' + accessToken)
      },

      onAfterResponse: async function (_, res) {
        const status = res.getStatus()
        if (status === 409) {
          // This status appears if the upload of a file has been cancelled unintentionally
          // and is continued after a browser reset (removal of the tus entries in the local storage).
          // In this conflict, the leftovers must be deleted first
          // and the upload will start from the beginning on the next retry.
          await this._deleteFileFromServer(this.file.uid)
        }
      }.bind(this),

      onError: function (error) {
        if (error.message.includes('failed to resume upload') && error.message.includes('response code: n/a')) {
          this.connectionIssueCallback()
        }
        this.status = 'failed'
      }.bind(this),

      onProgress: throttle(function (bytesUploaded, _) {
        this.uploadedBytes = bytesUploaded
      }.bind(this), 500),

      onSuccess: function () {
        this.status = 'uploaded'
        this.uploadedBytes = this.totalBytes
        destroyAnonymizedStream(this.file, this.fileGroup)
        this._deleteTusEntry(this.file.uid)
        this.singleFileUploadedCallback()
      }.bind(this)
    })
  }

  async _deleteFileFromServer (fileUid) {
    try {
      await us.deleteFile(fileUid)
    } catch (error) {
      console.log(error)
    }
  }

  _deleteTusEntry (fileId) {
    for (var i = 0; i < localStorage.length; i++) {
      if (localStorage.getItem(localStorage.key(i)).includes(fileId)) {
        localStorage.removeItem(localStorage.key(i))
      }
    }
  }
}

const fileStatusIconMapping = {
  queued: 'play_arrow',
  uploading: 'pause',
  waiting: 'pause',
  paused: 'play_arrow',
  failed: 'refresh',
  uploaded: 'check'
}

const getStatusIcon = function (upload) {
  return fileStatusIconMapping[upload.getStatus()]
}

const anonymizationStatusIconMapping = {
  checking: 'image_search',
  anonymizing: 'policy',
  'anonymization successful': 'verified_user',
  'anonymization failed': 'gpp_bad',
  'anonymization unsupported': 'gpp_maybe'
}

const getAnonymizationStatusIcon = function (upload) {
  return anonymizationStatusIconMapping[upload.getAnonymizationStatus()]
}

const getFileNameMetadata = function (file, fileGroup) {
  let fileName = file.uid + '.' + file.fileExtension
  if (!file.parentFile) {
    if (Object.keys(supportedMultiFileFormats).includes(file.fileExtension)) {
      fileName = supportedMultiFileFormats[file.fileExtension].getParentFilePath(file)
    }
  } else {
    if (Object.keys(supportedMultiFileFormats).includes(file.parentFile.fileExtension)) {
      fileName = supportedMultiFileFormats[file.parentFile.fileExtension].getLinkedFilePath(file)
    }
  }
  fileName = fileGroup + '/' + fileName
  if (getEnv('DMC_STATIC_SUBFOLDER')) {
    fileName = getEnv('DMC_STATIC_SUBFOLDER') + '/' + fileName
  }
  return fileName
}

export { UploadManager, fileStatusIconMapping, getStatusIcon, getAnonymizationStatusIcon, getFileNameMetadata }
