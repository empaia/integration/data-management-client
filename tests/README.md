# Tests

This subfolder runs the tests for all components of the Data Management Client.

## Run frontend tests with Playwright

Follow the quickstart with authentication and add the following environment variables (`.env`)

```bash
TEST_URL=http://localhost:8080
USERNAME=mta
PASSWORD=secret
TEST_DATA_DIR=/testdata/OpenSlide_adapted
```

Then run

```bash
yarn dev
cd tests
poetry install
poetry run pytest --envfile ../.env
```

Note: It is currently not possible to test the automatic upload of folders. Please test this manually before merging. (see [playwright issue](https://github.com/microsoft/playwright/issues/6854))

## Extend anonymization tests

In order to assure that the anonymization in the Data Management Client works for a various number of slides, the testcases within the `test_anonymization.py` can be extended by simply adding  slides to the `test_data_list.json` file following the given scheme.