import axios from 'axios'
import getEnv from './env'

const idmUrl = getEnv('IDM_URL')

const idm = {
  async alive () {
    await axios.get(idmUrl + '/alive')
  },
  // Case Mappings
  async addCaseIdMapping (empaiaId, localId) {
    const data = {
      empaia_id: empaiaId,
      mds_url: getEnv('MDS_URL'),
      local_id: localId
    }
    const response = await axios.post(idmUrl + '/v1/cases', data)
    return response.data
  },
  async getCaseIdMappingForEmpaiaId (empaiaId) {
    let localId = ''
    const response = await axios.get(idmUrl + `/v1/cases/empaia/${empaiaId}`)
    localId = response.data.local_id
    return localId
  },
  async getCaseIdMappingForEmpaiaIds (empaiaIds) {
    const response = await axios.put(idmUrl + '/v1/cases/empaia/query', { empaia_ids: empaiaIds })
    const mapping = {}
    for (const item of response.data.items) {
      mapping[item.empaia_id] = item.local_id
    }
    return mapping
  },
  async deleteCase (empaiaId) {
    const response = await axios.delete(idmUrl + `/v1/cases/empaia/${empaiaId}`)
    return response
  },
  async getCaseIdMappingForLocalId (localId) {
    const response = await axios.get(idmUrl + `/v1/cases/local/${localId}`)
    return response.data
  },
  // Slide Mappings
  async addSlideIdMapping (empaiaId, localId) {
    const data = {
      empaia_id: empaiaId,
      mds_url: getEnv('MDS_URL'),
      local_id: localId
    }
    const response = await axios.post(idmUrl + '/v1/slides', data)
    return response.data
  },
  async getSlideIdMappingForEmpaiaId (empaiaId) {
    const response = await axios.get(idmUrl + `/v1/slides/empaia/${empaiaId}`)
    return response.data
  },
  async deleteSlide (empaiaId) {
    const response = await axios.delete(idmUrl + `/v1/slides/empaia/${empaiaId}`)
    return response.data
  },
  async getSlideIdMappingForLocalId (localId) {
    const response = await axios.get(idmUrl + `/v1/slides/local/${localId}`)
    return response.data
  }
}

export default idm
