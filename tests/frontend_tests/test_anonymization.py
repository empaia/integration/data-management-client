import os

import pytest
from playwright.sync_api import Page
import time

from .helpers import (
  check_console_log_contains,
  create_case,
  delete_selected_case,
  get_acces_token,
  login_as_mta,
  select_case,
  setup_console_log,
  get_test_data,
)
from .test_upload import (
  add_files,
  delete_selected_case,
  get_acces_token,
  upload_with_select_slide,
  validate_anonymization,
  validate_case_deleted,
  validate_slide,
  validate_format,
  validate_tile,
  validate_region,
  validate_slide_deleted,
)

def pytest_generate_tests(metafunc):
    test_data = [slide for slide in get_test_data('test_data_list.json') if 'mrxs' not in slide[1]["name"]]
    if 'folder' and 'slide' in metafunc.fixturenames:
        metafunc.parametrize('folder, slide', test_data)

# Check manually: .mrxs
def test_anonymization_without_upload(page: Page, folder: str, slide: dict):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload and validate
    files = [os.path.join(os.environ["TEST_DATA_DIR"], folder, slide["name"])]
    upload_with_select_slide(page, files, start_upload=False, wait_for_upload=False)
    page.get_by_text("Start upload").wait_for()
    check_console_log_contains(console_log_path, "Automatic anonymization successful")
    access_token = get_acces_token(page)
    page.reload()
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_case_deleted(case_id, access_token)


def test_anonymization_without_upload_adding_while_anonymizing(page: Page):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload and validate
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Ventana", "OS-2.bif")]
    add_files(page, files)
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    add_files(page, files)
    page.get_by_text("Start upload").wait_for()
    check_console_log_contains(console_log_path, "Automatic anonymization successful")
    access_token = get_acces_token(page)
    page.reload()
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_case_deleted(case_id, access_token)


# Check manually: .mrxs
def test_anonymization_with_upload_only_format(page: Page, folder: str, slide: dict):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload
    files = [os.path.join(os.environ["TEST_DATA_DIR"], folder, slide["name"])]
    uploaded_slide_ids = upload_with_select_slide(page, files)
    slide_id = uploaded_slide_ids[0]
    access_token = get_acces_token(page)
    # validate
    validate_slide(case_id, slide_id, access_token)
    validate_format(slide_id, slide["format"], access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)

# Check manually: .mrxs
def test_anonymization_with_upload_label_and_macro(page: Page, folder: str, slide: dict):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload
    files = [os.path.join(os.environ["TEST_DATA_DIR"], folder, slide["name"])]
    uploaded_slide_ids = upload_with_select_slide(page, files)
    slide_id = uploaded_slide_ids[0]
    access_token = get_acces_token(page)
    # validate
    validate_slide(case_id, slide_id, access_token)
    validate_anonymization(slide_id, "label", slide["label_expected"], console_log_path, access_token)
    validate_anonymization(slide_id, "macro", slide["macro_expected"], console_log_path, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)


# Check manually: .mrxs
def test_anonymization_with_upload_tile_and_region(page: Page, folder: str, slide: dict):
    console_log_path = setup_console_log(page)
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    # upload
    files = [os.path.join(os.environ["TEST_DATA_DIR"], folder, slide["name"])]
    uploaded_slide_ids = upload_with_select_slide(page, files)
    slide_id = uploaded_slide_ids[0]
    access_token = get_acces_token(page)
    # validate
    validate_slide(case_id, slide_id, access_token)
    validate_tile(slide_id, slide["test_tile"], access_token)
    validate_region(slide_id, slide["test_region"], access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)