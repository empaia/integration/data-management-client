import axios from 'axios'
import getEnv from './env'

const usUrl = getEnv('US_URL')

const us = {
  async deleteFile (fileId) {
    const response = await axios.delete(usUrl + `/v1/files/${fileId}`)
    return response
  }
}

export default us
