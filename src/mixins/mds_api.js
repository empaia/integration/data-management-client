import axios from 'axios'
import getEnv from './env'

const mdsUrl = getEnv('MDS_URL')

const mds = {
  // Clinical Data Service - Cases
  async createCase (creatorId, description) {
    const data = {
      creator_id: creatorId,
      description: description,
      creator_type: 'USER'
    }
    const response = await axios.post(mdsUrl + '/private/v3/cases', data)
    return response.data
  },
  async getCase (caseId) {
    const response = await axios.get(mdsUrl + `/v3/cases/${caseId}`)
    return response.data
  },
  async getAllCases () {
    const response = await axios.get(mdsUrl + '/v3/cases')
    return response.data.items
  },
  async updateCase (caseId, description) {
    const data = {
      description: description
    }
    const response = await axios.put(mdsUrl + `/private/v3/cases/${caseId}`, data)
    return response.data
  },
  async deleteCase (caseId) {
    const data = {
      deleted: true
    }
    const response = await axios.put(mdsUrl + `/private/v3/cases/${caseId}`, data)
    return response.data
  },
  // Clinical Data Service - Slides
  async createSlide (data) {
    const response = await axios.post(mdsUrl + '/private/v3/slides', data)
    return response.data
  },
  async getSlide (slideId) {
    const response = await axios.get(mdsUrl + `/v3/slides/${slideId}`)
    return response.data
  },
  async getAllSlides (caseId) {
    const response = await axios.put(mdsUrl + '/v3/slides/query', { cases: [caseId] })
    return response.data.items
  },
  async updateSlide (slideId, tissue = '', stain = '', block = '') {
    const data = {
      tissue: tissue,
      stain: stain,
      block: block
    }
    const response = await axios.put(mdsUrl + `/private/v3/slides/${slideId}`, data)
    return response.data
  },
  async deleteSlide (slideId) {
    const data = {
      deleted: true
    }
    const response = await axios.put(mdsUrl + `/private/v3/slides/${slideId}`, data)
    return response.data
  },
  // old WSI Service
  async getSlideInfo (slideId) {
    const response = await axios.get(mdsUrl + `/v3/slides/${slideId}/info`)
    return response.data
  },
  getTileUrl (slideId, level, x, y, z) {
    return mdsUrl + `/v3/slides/${slideId}/tile/level/${level}/tile/${x}/${y}?z=${z}`
  },
  async getThumbnail (slideId) {
    const response = await axios.get(mdsUrl + `/v3/slides/${slideId}/thumbnail/max_size/400/400`, { responseType: 'arraybuffer' })
    return 'data:image/jpeg;base64,' + Buffer.from(response.data, 'binary').toString('base64')
  },
  // Examination Service
  async createPreprocessingRequest (userId, slideId) {
    const data = {
      creator_id: userId,
      creator_type: 'USER',
      slide_id: slideId
    }
    const response = await axios.post(mdsUrl + '/v3/preprocessing-requests', data)
    return response.data
  },
  async queryPreprocessingRequests () {
    const response = await axios.put(mdsUrl + '/v3/preprocessing-requests/query', {})
    return response.data
  },
  // Storage Mapper Service
  async addStorageMapping (slideId, data) {
    const response = await axios.put(mdsUrl + `/private/v3/slides/${slideId}/storage`, data)
    return response.data
  },
  async getStorageMapping (slideId) {
    const response = await axios.get(mdsUrl + `/private/v3/slides/${slideId}/storage`)
    return response.data
  },
  async deleteStorageMapping (slideId) {
    await axios.delete(mdsUrl + `/private/v3/slides/${slideId}/storage`)
  }
}

export default mds
