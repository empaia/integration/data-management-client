FROM node:16 as build_stage

RUN yarn global add @quasar/cli
COPY . /source
WORKDIR /source
RUN yarn install --frozen-lockfile
RUN quasar build


FROM nginx:1.27.4-alpine@sha256:4ff102c5d78d254a6f0da062b3cf39eaf07f01eec0927fd21e219d0af8bc0591

RUN apk add --upgrade curl freetype
COPY --from=build_stage /source/dist/* /app
COPY entrypoint.sh /usr/share/nginx/
ENTRYPOINT ["/usr/share/nginx/entrypoint.sh"]
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
