# Changelog

## 0.14.10

- Updated dependencies

## 0.14.9

- Updated dependencies

## 0.14.8

- Updated dependencies

## 0.14.7

- Updated dependencies
- Updated @babel/eslint-parser to 7.26.8

## 0.14.6

- Updated dependencies
- replace docker-compose with docker compose
- upgraded Python to ^3.12

## 0.14.5

- Updated dependencies

## 0.14.4

- Updated dependencies

## 0.14.3

- fix frontend-tests

## 0.14.2

- fix DMC_DISABLE_PREPROCESSING_CONTROLS

## 0.14.1

- allow disabling/enabling of preprocessing on a slide via environment variable

## 0.14.0

- persist original_path of wsi in storage address on upload (depends on CDS v0.6.3 and MDS v0.17.7)

## 0.13.9

- Updated dependencies

## 0.13.8

- Updated dependencies

## 0.13.7

- Updated dependencies

## 0.13.6

- Updated dependencies

## 0.13.5

- Updated dependencies

## 0.13.4

- Updated dependencies

## 0.13.3

- Updated dependencies
- Updated nginx version from 1.27.0-alpine to 1.27.1-alpine

## 0.13.2

- Updated dependencies
- Updated @babel/eslint-parser to 7.25.1

## 0.13.1

- Updated dependencies

## 0.13.0

- Added slide viewer functionality

## 0.12.61

- Updated dependencies

## 0.12.60

- Updated dependencies

## 0.12.59

- Updated dependencies

## 0.12.58

- Updated dependencies

## 0.12.57

- Updated dependencies

## 0.12.56

- Updated dependencies

## 0.12.55

- Updated dependencies

## 0.12.54

- Updated dependencies

## 0.12.53

- Updated dependencies

## 0.12.52

- Updated dependencies

## 0.12.51

- Updated dependencies

## 0.12.50

- Updated dependencies

## 0.12.49

- Updated dependencies

## 0.12.48

- Updated dependencies

## 0.12.47

- Updated dependencies
- Updated @babel/eslint-parser to v7.24.1
- integrate changes of clinical-data-service

## 0.12.46

- Updated id-mapper-service

## 0.12.45

- Updated dependencies

## 0.12.44

- Updated dependencies

## 0.12.43

- Updated dependencies

## 0.12.42

- integrate clinical-data-service with new changes

## 0.12.41

- Updated dependencies

## 0.12.40

- Updated @babel/eslint-parser to v7.23.10

## 0.12.39

- extend files for anonymization tests
- extend list of supported file formats for anonymization by Philips' TIFF

## 0.12.38

- Updated dependencies

## 0.12.37

- Updated dependencies

## 0.12.36

- add tooltips to buttons

## 0.12.35

- Updated dependencies

## 0.12.34

- add effect for dropzone

## 0.12.33

- Updated vue to v2.7.15
- Updated isyntax registry URL

## 0.12.32

- Updated dependencies

## 0.12.31

- Updated dependencies and changed renovate.json

## 0.12.30

- add testdata to runner and extend testcases
- validate white images for specific image type (e.g. label image) if they are expected

## 0.12.29

- Updated dependencies

## 0.12.28

- manually updated dependencies and pin packages

## 0.12.27

- add anonymization tests for isyntax

## 0.12.26

- Updated @babel/eslint-parser to v7.23.3

## 0.12.25

- Updated dependencies

## 0.12.24

- Updated dependencies

## 0.12.23

- updated wsi-anon

## 0.12.22

- validate slide format in anonymization tests
- validate specific region in anonymization tests
- validate specific tile in anonymization tests

## 0.12.21

- replace static parameters in test_anonymization.py with json file

## 0.12.20

- Updated dependencies

## 0.12.19

- Updated dependencies

## 0.12.18

- Updated dependencies

## 0.12.17

- Updated dependencies

## 0.12.16

- Changed empaia logo
- Improved case loading
- Added case mapping deletion

## 0.12.15

- Updated dependencies

## 0.12.14

- Updated dependencies

## 0.12.13

- Updated dependencies

## 0.12.11

- Updated dependencies

## 0.12.10

- Updated dependencies

## 0.12.9

- Updated dependencies

## 0.12.8

- Updated dependencies

## 0.12.7

- Updated wsi-anon tool

## 0.12.6

- Updated dependencies (wsi-anon)
- reduced default refresh to 2.5 min

## 0.12.5

- Updated dependencies

## 0.12.4

- Re-enabled anonymization for mrxs files

## 0.12.3

- Disabled anonymization for mrxs files

## 0.12.2

- Updated dependencies

## 0.12.1

- Updated dependencies

## 0.12.0

- Added anonymization suppport to UI
- Added extended vsf filename anonymization
- Updated wsi-anon to 0.4.0
- Fixed parallel anonymization of files with the same name

## 0.11.0

- Added internal UploadManager
- Switched to sequential file upload
- Introduced new upload status: WAITING
- Fixed already added file check
- Enabled parallel file upload to different cases
- Fixed parallel upload of same file to different cases
- Activated anonymization for mrxs, isyntax, bif
- Adapted header to match other empaia components
- Added "Add new slide" button to show slide dropzone
- Removed automatic upload option
- Enabled failed uploads to resume
- Added connection issue notification
- Added upload status to header

## 0.10.10

- Updated dependencies

## 0.10.9

- Updated dependencies

## 0.10.8

- Updated dependencies

## 0.10.7

- Updated vue

## 0.10.6

- Fixed preprocessing creator id

## 0.10.5

- Updated dependencies

## 0.10.4

- Added hidden logging mode

## 0.10.3

- Updated dependencies

## 0.10.2

- Updated oidc-client-ts to 2.2.1
- Fixed file + folder drop
- Added env for refresh token setting

## 0.10.1

- Added supported anonymization format check
- Updated wsi-anon to 0.3.13
- Fixed multi-file upload

## 0.10.0

- Updated filtered file drop
- Refactored components
- Enabled adding additional files while uploading
- Added automatic upload option
- Moved case creation to dialog
- Added ID overview on hover
- Added copy to clipboard of slide id if clicked

## 0.9.1

- Updated dependencies

## 0.9.0

- Added frontend tests based on playwright
- Added file and folder selection
- Added frontend tests for uploading
- Removed configuration issue note
- Fixed cancel all for multi file slides
- Added duplicate filter
- Sort existing slides by creation date
- Improved readability of remaining upload time
- Added DICOM upload support
- Added possibility to cancel queued files before upload

## 0.8.3

- Updated dependencies

## 0.8.2

- Updated dependencies

## 0.8.1

- Updated dependencies

## 0.8.0

- Added preprocessing trigger
- Updated dependencies
- Use updated MDS private routes
- Use MPS for tissue and stain tags

## 0.7.1

- Renamed "Anmelden" to "Login"
- Added reponse error check
- Added error message for "API access denied"
- Added DMC_ORGANIZATION_NAME env
- Reverted MDS private routes
- Reverted service versions to fit iron deployment

## 0.7.0

- Use oidc client library
- Updated dependencies
- Added keycloak to docker compose
- Changed frontend tests to work with keycloak

## 0.6.31

- Use updated MDS private routes

## 0.6.30

- Updated dependencies

## 0.6.29

- Updated dependencies

## 0.6.28

- Updated dependencies

## 0.6.24

- Updated dependencies

## 0.6.23

- Updated dependencies

## 0.6.22

- Updated dependencies

## 0.6.21

- Added support for VSF slide format

## 0.6.20

- Updated dependencies

## 0.6.19

- Added proper handling of unsupported slides

## 0.6.18

- Updated dependencies

## 0.6.17

- Exchanged logo and minor adaptions to header layout

## 0.6.16

- Updated dependencies

## 0.6.15

- Anonymization no longer advertised
- Blocking all navigation options during upload
- Added slide count

## 0.6.14

- Updated dependencies
- Fixed upload service setup

## 0.6.13

- Updated CI

## 0.6.12

- Removed fudge tool
- Adapted support mail

## 0.6.11

- Added full name to header
- Refined agreement dialogs
- Added more precise error for upload of the same or not supported files

## 0.6.10

- Fixed token refresh during upload

## 0.6.9

- Adapted upload to work with refresh token

## 0.6.8

- Disabled fixed header

## 0.6.7

- Added fudge tool hardcoded

## 0.6.6

- Use well-known url to retrieve auth and token url

## 0.6.5

- Added fudge tool
- Added help section
- Added warnings before case creation and upload start
- Adapted header to portal

## 0.6.4

- Updated services
- Added notification in case of misconfigured upload service
- Updated screenshots

## 0.6.3

- Fixed typo

## 0.6.2

- Renamed MDS UI to Data Management Client
- Added subfolder variable DMC_STATIC_SUBFOLDER
- Added case id based subfolders
- Disabled case switch while uploading

## 0.6.1

- Refactored code
- Added FilteredFileDrop class

## 0.6.0

- Added dialog to confirm or remove slides after failed anonymization
- Simplified upload, removed selfcontained version
- Added check for already uploaded files
- File IDs now based on name, size, lastModified
- Check if already uploaded based on new file IDs
- Resume files even after browser restart

## 0.5.1

- Upgrade to wsi-anon 0.3.0

## 0.5.0

- Added case and slide deletion
- Added filehash for local id
- Added slide search
- Added slide anonymization

## 0.4.6

- Use tag definitions introduced by clinical data service

## 0.4.5

- Added parallel slide loading

## 0.4.4

- Fixed id used for case creation
- Slightly adapted UI to match EMPAIA Portal

## 0.4.3

- Fixed missing CLIENT_ID
- Fixed slide name length and added tooltip

## 0.4.2

- Updated services
- Adapted to new clinical data service responses

## 0.4.1

- Added notification for failed requests
- Show available cases and slides even if some of them are not available

## 0.4.0

- Added more supported formats
- Added list of supported formats to drop zone
- Added warning if none of the dropped files is supported

## 0.3.0

- Migrated to new MDS and updated all services

## 0.2.0

- Change configurable service URLs to be roots without API prefix

## 0.1.1

- Added frontend tests

## 0.1.0

- Show local slide id on update
- Extended readme

## 0.0.11

- Fixed MDS_UI_STATIC_USER_ID usage
- Added docker compose file for no auth

## 0.0.10

- Fixed upload abort on case change

## 0.0.9

- Enabled auth for Upload Service

## 0.0.8

- Enabled auth for ID Mapper Service

## 0.0.7

- Update token with refresh token once it is expired
- Added CLIENT_ID variable

## 0.0.6

- Added MDS_UI_STATIC_USER_ID, if set, authentication is disabled
- Fixed problem with authentication, redirect_url und hash mode of vue router

## 0.0.5

- Remove MDS_UI_BASE_HREF as it did not work with history router mode
- Switch back to hash router mode to allow for arbitrary relative deployments

## 0.0.4

- Add MDS_UI_BASE_HREF for reverse proxy deployment
- Remove MDS_UI_URL used for redirect in favor of window.location.href
- Add ENV section to Readme

## 0.0.3

- Added profile link
- Added token refresh

## 0.0.2

- Added loading spinner for slides
