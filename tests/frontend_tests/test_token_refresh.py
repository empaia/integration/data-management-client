import os
import time

from playwright.sync_api import Page

from .helpers import create_case, get_decoded_access_token, login_as_mta, select_case
from .test_upload import (
  delete_selected_case,
  get_acces_token,
  get_uploaded_slides,
  upload_with_select_slide,
  validate_case_deleted,
  validate_slide,
  validate_slide_deleted,
  wait_for_upload_to_finish,
)


def set_token_lifespan(page, acccess_token_lifespan, sso_session_idle):
    page.goto("http://localhost:10082/auth/")
    page.get_by_role("link", name="Administration Console").click()
    page.get_by_label("Username or email").click()
    page.get_by_label("Username or email").fill("admin")
    page.get_by_label("Username or email").press("Tab")
    page.get_by_label("Password", exact=True).fill("admin")
    page.get_by_label("Password", exact=True).press("Enter")
    page.get_by_test_id("realmSelectorToggle").click()
    page.get_by_role("menuitem", name="EMPAIA").click()
    page.get_by_role("link", name="Realm settings").click()
    page.get_by_test_id("rs-tokens-tab").click()
    if int(page.get_by_test_id("access-token-lifespan-input").input_value()) != acccess_token_lifespan:
        page.get_by_test_id("access-token-lifespan-input").fill(str(acccess_token_lifespan))
        page.get_by_test_id("access-token-lifespan-input").press("Enter")
        page.get_by_test_id("tokens-tab-save").click()
    page.get_by_test_id("rs-sessions-tab").click()
    if int(page.get_by_test_id("sso-session-idle-input").input_value()) != sso_session_idle:
        page.get_by_test_id("sso-session-idle-input").fill(str(sso_session_idle))
        page.get_by_test_id("sso-session-idle-input").press("Enter")
        page.get_by_test_id("sessions-tab-save").click()


def get_current_token_lifespan(page):
    token_dict = get_decoded_access_token(page)
    return token_dict["exp"] - token_dict["iat"]


def get_current_token_remaining_time(page):
    token_dict = get_decoded_access_token(page)
    return token_dict["exp"] - time.time()


def test_token_refresh(page: Page):
    token_lifespan_in_min = 1
    set_token_lifespan(
        page,
        token_lifespan_in_min,
        15
    )
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    assert get_current_token_lifespan(page) == token_lifespan_in_min * 60
    time_to_expire = get_current_token_remaining_time(page)
    time.sleep(time_to_expire + 1)
    page.reload()
    page.get_by_text(case_name).wait_for()
    assert get_current_token_remaining_time(page) > 0
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()


def test_network_off_token_expired_refresh_token_valid(page: Page, context):
    token_lifespan_in_min = 1
    set_token_lifespan(
        page,
        token_lifespan_in_min,
        2
    )
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    assert get_current_token_lifespan(page) == token_lifespan_in_min * 60
    time_to_expire = get_current_token_remaining_time(page)
    context.set_offline(True)
    time.sleep(time_to_expire + 5)
    context.set_offline(False)
    page.get_by_text(f"Connection re-established").wait_for()
    context.set_offline(False)
    assert get_current_token_remaining_time(page) > 0
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()


def test_network_off_refresh_token_expired(page: Page, context):
    token_lifespan_in_min = 1
    set_token_lifespan(
        page,
        token_lifespan_in_min,
        token_lifespan_in_min
    )
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    assert get_current_token_lifespan(page) == token_lifespan_in_min * 60
    time_to_expire = get_current_token_remaining_time(page)
    context.set_offline(True)
    time.sleep(time_to_expire + 5)
    assert get_current_token_remaining_time(page) < 0
    page.get_by_text(f"Refresh token expired due to connection issues").wait_for()
    context.set_offline(False)
    page.get_by_role("button", name="Reload Page").click()
    page.get_by_text(f"You are currently not logged in").wait_for()


def test_token_refresh_while_uploading(page: Page):
    token_lifespan_in_min = 1
    set_token_lifespan(
        page,
        token_lifespan_in_min,
        15
    )
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    assert get_current_token_lifespan(page) == token_lifespan_in_min * 60
    time_to_expire = get_current_token_remaining_time(page)
    # upload, pause (> token lifespan), continue upload and validate
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    uploaded_slide_ids = upload_with_select_slide(
        page, files, extended_pause_in_seconds=time_to_expire + 1
    )
    slide_id = uploaded_slide_ids[0]
    access_token = get_acces_token(page)
    validate_slide(case_id, slide_id, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)


def test_token_refresh_while_uploading_network_offline(page: Page, context):
    token_lifespan_in_min = 1
    set_token_lifespan(
        page,
        token_lifespan_in_min,
        15
    )
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_name = create_case(page)
    select_case(page, case_name)
    case_id = page.url.split("/")[-1]
    assert get_current_token_lifespan(page) == token_lifespan_in_min * 60
    time_to_expire = get_current_token_remaining_time(page)
    # upload, pause (> token lifespan), continue upload and validate
    files = [os.path.join(os.environ["TEST_DATA_DIR"], "Aperio", "CMU-1.svs")]
    upload_with_select_slide(page, files, wait_for_upload=False)
    time.sleep(1)
    context.set_offline(True)
    time.sleep(time_to_expire + 1)
    context.set_offline(False)
    wait_for_upload_to_finish(page, [], files)
    slide_id = get_uploaded_slides(page)[0]
    access_token = get_acces_token(page)
    validate_slide(case_id, slide_id, access_token)
    # delete case
    delete_selected_case(page)
    page.get_by_text(f"Case {case_name} deleted").wait_for()
    validate_slide_deleted(case_id, slide_id, access_token)
    validate_case_deleted(case_id, access_token)
