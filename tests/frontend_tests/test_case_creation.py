import os
from uuid import uuid4

from playwright.sync_api import Page, expect

from .helpers import create_case, delete_selected_case, login_as_mta, select_case


def test_case_creation_and_deletion(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_id = create_case(page)
    select_case(page, case_id)
    delete_selected_case(page)


def test_case_creation_empty_input(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    page.locator("#start-case-creation").click()
    page.get_by_role("button", name="Create Case").click()
    page.get_by_text("Please add ID.").wait_for()
    page.get_by_label("ID").fill(str(uuid4()))
    page.get_by_role("button", name="Create Case").click()
    page.get_by_text("Please add a description.").wait_for()


def test_case_creation_already_exists(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_id = create_case(page)
    page.locator("#start-case-creation").click()
    page.get_by_label("ID").fill(case_id)
    page.locator("#case-creation-form").get_by_label("Description").click()
    page.locator("#case-creation-form").get_by_label("Description").fill("test")
    page.get_by_text("Please change ID. ID already exists.").wait_for()
    page.get_by_role("button", name="Cancel").click()
    select_case(page, case_id)
    delete_selected_case(page)


def test_case_creation_cancel(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_id = create_case(page)
    page.locator("#start-case-creation").click()
    page.get_by_label("ID").fill(case_id)
    page.locator("#case-creation-form").get_by_label("Description").click()
    page.locator("#case-creation-form").get_by_label("Description").fill("test")
    page.get_by_text("Please change ID. ID already exists.").wait_for()
    page.get_by_role("button", name="Cancel").click()
    page.locator("#start-case-creation").click()
    expect(page.locator("#case-creation-form").get_by_label("ID")).to_have_value("")
    expect(
        page.locator("#case-creation-form").get_by_label("Description")
    ).to_have_value("")


def test_agreement_not_showing_again(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    case_id_1 = create_case(
        page, click_agreement=True, click_agreement_not_show_again=True
    )
    case_id_2 = create_case(
        page, click_agreement=False, click_agreement_not_show_again=False
    )
    select_case(page, case_id_2)
    delete_selected_case(page)
    select_case(page, case_id_1)
    delete_selected_case(page)
