import io
import json
import os
import tempfile
import time
from uuid import uuid4

import jwt
import numpy as np
from PIL import Image
from pathlib import Path
import json


def get_data_path():
    file_dir = os.path.dirname(os.path.abspath(__file__))
    return os.path.join(file_dir, "data")

def get_test_data(filename):
    folder_path = os.path.abspath(Path(os.path.dirname(__file__)))
    folder = os.path.join(folder_path, "..")
    jsonfile = os.path.join(folder, filename)
    with open(jsonfile) as file:
        data = json.load(file)
        # structure for each slide in returned data = (folder, slide{name, format, ...})
        return [(folder, slide) for folder in data for slide in data[folder]]

def login_as_mta(
    page,
    username=os.environ["USERNAME"],
    password=os.environ["PASSWORD"],
    timeout=30000,
):
    page.get_by_role("button", name="Login").click()
    page.get_by_label("Username or email").click()
    page.get_by_label("Username or email").fill(username)
    page.get_by_label("Username or email").press("Tab")
    page.get_by_label("Password", exact=True).fill(password)
    page.get_by_role("button", name="Sign In").click()
    page.locator("#start-case-creation").wait_for(timeout=timeout)


def compare_screenshot_of_page_to_regression_data(page, regression_data_filename):
    screenshot = Image.open(io.BytesIO(page.screenshot()))
    regression_data_path = os.path.join(get_data_path(), regression_data_filename)
    if not os.path.exists(regression_data_path):
        screenshot.save(regression_data_path)
    screenshot_regression = Image.open(regression_data_path)
    assert np.allclose(np.array(screenshot), np.array(screenshot_regression))


def create_case(page, click_agreement=True, click_agreement_not_show_again=False):
    page.locator("#start-case-creation").click()
    page.get_by_label("ID").click()
    case_id = str(uuid4())
    page.get_by_label("ID").fill(case_id)
    page.locator("#case-creation-form").get_by_label("Description").click()
    page.locator("#case-creation-form").get_by_label("Description").fill("test")
    page.get_by_role("button", name="Create Case").click()
    if click_agreement_not_show_again:
        page.get_by_role(
            "checkbox",
            name="Please do not display this warning again. I will always follow the agreement when adding new data.",
        ).click()
    if click_agreement:
        page.get_by_role("button", name="Agree").click()
    page.get_by_text("Case " + case_id + " created").wait_for()
    return case_id


def select_case(page, case_id):
    time.sleep(1)
    page.get_by_role("combobox", name="Search and select existing cases").click()
    page.get_by_role("listitem").filter(has_text=case_id).click()


def delete_selected_case(page):
    page.locator("#delete_case").first.click()
    page.get_by_role("button", name="Confirm").click()


def get_acces_token(page):
    return json.loads(
        json.loads(page.evaluate("() => JSON.stringify(sessionStorage)"))[
            "oidc.user:http://localhost:10082/auth/realms/EMPAIA:MDC_CLIENT"
        ]
    )["access_token"]


def get_decoded_access_token(page):
    access_token = get_acces_token(page)
    return jwt.decode(access_token, options={"verify_signature": False})


def setup_console_log(page):
    _, path = tempfile.mkstemp()

    def add_to_console(msg):
        with open(path, mode="a") as f:
            f.write(str(msg))

    page.on("console", lambda msg: add_to_console(msg))

    return path


def check_console_log_contains(path, contained_string):
    with open(path, mode="r") as f:
        console_log = f.read()
    assert contained_string in console_log
