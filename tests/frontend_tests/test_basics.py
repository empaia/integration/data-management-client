import os

import pytest
from playwright.sync_api import Page
from playwright.sync_api import TimeoutError as PlaywrightTimeoutError
from playwright.sync_api import expect

from .helpers import get_data_path, login_as_mta


def test_research_only(page: Page):
    page.goto(os.environ["TEST_URL"])
    expect(page).to_have_title("Data Management Client")
    login_as_mta(page)
    assert (
        "For research purposes only: Data must be anonymized and approved for research purposes before uploading."
        in page.content()
    )


def test_login_logout_valid(page: Page):
    page.goto(os.environ["TEST_URL"])
    login_as_mta(page)
    assert page.locator("#start-case-creation").is_visible()
    page.locator("#menu").click()
    page.get_by_text("Logout", exact=True).click()
    page.get_by_role("button", name="Login").wait_for()


def test_login_invalid(page: Page):
    page.goto(os.environ["TEST_URL"])
    with pytest.raises(PlaywrightTimeoutError):
        login_as_mta(page, username="unknown", timeout=1000)
    assert "Invalid username or password" in page.content()


def test_ui_regression(page: Page, browser_name):
    page.goto(os.environ["TEST_URL"])
    page.get_by_role("button", name="Login").wait_for()
    page.screenshot(path=os.path.join(get_data_path(), f"ui_login_{browser_name}.png"))
    # todo check against job artifact from main
