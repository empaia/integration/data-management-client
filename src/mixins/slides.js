import mds from './mds_api'
import idm from './idm_api'
import us from './us_api'
import { v5 as uuidv5 } from 'uuid'
import { supportedMultiFileFormats } from './file_formats'

function getLabel (slide, limitLength = true) {
  let filename = ''
  if (slide.localId) {
    filename = localStorage.getItem(slide.localId) || slide.localId
  }
  if (slide.fileBase) {
    filename = slide.fileBase + '.' + slide.fileExtension
  }
  if (slide.folder) {
    filename = slide.folder
  }
  if (limitLength) {
    if (filename.length > 14) {
      filename = filename.substring(0, 14) + ' ...'
    }
  }
  return filename
}

const getUpdatedSlidePromise = function (slide) {
  const p1 = new Promise(function (resolve) {
    resolve(idm.getSlideIdMappingForEmpaiaId(slide.id).catch((error) => {
      console.log('Unable to receive mapping for slide id:', slide.id, error)
      return null
    }))
  })
  const p2 = new Promise(function (resolve) {
    resolve(mds.getStorageMapping(slide.id).catch((error) => {
      console.log('Unable to receive storage mapping for slide id:', slide.id, error)
      return null
    }))
  })
  const p3 = new Promise(function (resolve) {
    resolve(mds.getSlideInfo(slide.id).catch((error) => {
      console.log('Unable to receive slide info for slide id:', slide.id, error)
      return null
    }))
  })
  const p4 = new Promise(function (resolve) {
    resolve(mds.getThumbnail(slide.id).catch((error) => {
      console.log('Unable to receive slide thumbnail for slide id:', slide.id, error)
      return null
    }))
  })
  return Promise.all([p1, p2, p3, p4]).then((values) => {
    if (values[0]) {
      slide.localId = values[0].local_id
    } else {
      slide.localId = 'UNKNOWN'
    }
    slide.storage = values[1]
    slide.info = values[2]
    slide.thumbnail = values[3]
    return slide
  })
}

const deleteSlide = async function (slide) {
  const storageMapping = await mds.getStorageMapping(slide.id).catch((error) => {
    console.log('Unable to get storage mapping for slide id:', slide.id, error)
    return null
  })
  if (storageMapping) {
    const mainFile = storageMapping.main_storage_address
    await us.deleteFile(mainFile.storage_address_id).catch((error) => {
      console.log('Unable to delete the main file with id:', mainFile.storage_address_id, error)
    })
    for (const address of storageMapping.secondary_storage_addresses) {
      await us.deleteFile(address.storage_address_id).catch((error) => {
        console.log('Unable to delete the secondary file with id:', address.storage_address_id, error)
      })
    }
    await mds.deleteStorageMapping(slide.id).catch((error) => {
      console.log('Unable to delete storage mapping for slide id:', slide.id, error)
    })
  }
  await idm.deleteSlide(slide.id).catch((error) => {
    console.log('Unable to delete mapping for slide id:', slide.id, error)
  })
  await mds.deleteSlide(slide.id).catch((error) => {
    console.log('Unable to delete slide for slide id:', slide.id, error)
  })
}

const addSlideFromFinishedUpload = async function (multiFileUpload, localCaseId) {
  const caseId = multiFileUpload.fileGroup
  const uploadedFile = _getUploadedFile(multiFileUpload)
  const slideResponse = await mds.createSlide({ case_id: caseId })
  let mainStorageAddress = {
    storage_address_id: uploadedFile.fileId,
    path: uploadedFile.name,
    original_path: uploadedFile.originalPath
  }
  const secondaryStorageAddresses = []
  const fileExtension = multiFileUpload.file.fileExtension
  if (Object.keys(supportedMultiFileFormats).includes(fileExtension)) {
    if (supportedMultiFileFormats[fileExtension].folderFormat) {
      const address = uploadedFile.name.substring(0, uploadedFile.name.lastIndexOf('/'))
      mainStorageAddress = {
        storage_address_id: uuidv5(uploadedFile.name, caseId),
        path: address,
        original_path: uploadedFile.originalPath
      }
    }
    for (const linkedFile of uploadedFile.linkedFiles) {
      secondaryStorageAddresses.push({
        storage_address_id: linkedFile.fileId,
        path: linkedFile.name,
        original_path: linkedFile.originalPath
      })
    }
  }
  const storageMapping = {
    main_storage_address: mainStorageAddress,
    secondary_storage_addresses: secondaryStorageAddresses
  }
  await mds.addStorageMapping(slideResponse.id, storageMapping)
  const fileHash = uuidv5(localCaseId + uploadedFile.localId, caseId)
  await idm.addSlideIdMapping(slideResponse.id, fileHash)
  localStorage.setItem(fileHash, uploadedFile.localId)

  return [slideResponse.id, uploadedFile.localId]
}

const _getUploadedFile = function (multiFileUpload) {
  const file = multiFileUpload.file
  let name = ''
  let localId = file.fileBase + '.' + file.fileExtension
  if (file.folder) {
    localId = file.folder
  }
  const linkedFiles = []
  for (const upload of multiFileUpload.uploads) {
    if (upload.metadata.id === file.uid) {
      name = upload.metadata.file_name
    } else {
      linkedFiles.push({
        name: upload.metadata.file_name,
        fileId: upload.metadata.id,
        originalPath: upload.file.fullPath
      })
    }
  }
  return {
    name: name,
    localId: localId,
    linkedFiles: linkedFiles,
    fileId: file.uid,
    originalPath: file.fullPath
  }
}

export { getLabel, getUpdatedSlidePromise, deleteSlide, addSlideFromFinishedUpload }
