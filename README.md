# Data Management Client

The Data Management Client enables the creation of cases and upload of slides. Its focus is the simple anonymization and upload of slides in different formats (currently supported formats are defined in [file_formats.js](src/mixins/file_formats.js)). All files corresponding to one or several slides can be dropped in the *"Drop slides here"*-Area. After the upload of a slide is finished, tissue and stain for a slide can be defined. It is furthermore possible to inspect a slide more closely via the integrated slide viewer, which can be accessed by clicking on a slides thumbnail (if given).

The Data Management Client needs a number of services to work (see [docker-compose.yml](docker-compose.yml)):

* Medical Data Service
  * Clinical Data Service
* ID Mapper Service
* Upload Service
* Marketplace Service
* Examination Service
* Keycloak

### Example test case

![Screenshot](./images/screen_testcase.png)

### Example slide viewer

![Screenshot](./images/screen_slideviewer.png)

## Quickstart

The Data Management Client can be configured with or without authentication.

### Without Authentication

Copy and rename the `sample_env` file to `.env`. It contains the following environment variables:

```bash
MDS_URL=http://localhost:10006
US_URL=http://localhost:10005
IDM_URL=http://localhost:10007
MPS_URL=http://localhost:10010
DMC_STATIC_SUBFOLDER=
DMC_DISABLE_PREPROCESSING_CONTROLS=false
DMC_STATIC_USER_ID=TEST_USER
```

Start the Data Management Client by running:

```bash
docker compose -f docker-compose.yml -f docker-compose-no-auth.yml up
```
### With Authentication

Copy and rename the `sample_env` file to `.env`. Change and add the following environment variables:

```bash
DMC_STATIC_USER_ID=
AUTH_URL=http://localhost:10082/auth/realms/EMPAIA
CLIENT_ID=MDC_CLIENT
```

Start the Data Management Client by running:

```bash
docker compose up
```

The user interface is available at `http://localhost:10054`.

## Configuration of Docker container

The docker image accepts environment variables for the configuration of serveral services and base href:

* `MDS_URL`: URL to the Medical Data Service (without API version)
* `US_URL`: URL to the Upload Service (without API version)
* `IDM_URL`: URL to the Id Mapping Service (without API version)
* `MPS_URL`: URL to the Marketplace Service (without API version)
* `AUTH_URL`: URL for client authentication
* `CLIENT_ID`: ID for client authentication
* `DMC_STATIC_USER_ID`: ID used for data creation, if set, authentication is disabled
* `DMC_STATIC_SUBFOLDER`: Subfolder that will be used to store files at the Upload Service

## Development

The Data Management Client consists of several Vue components:

```mermaid
graph TD;
  Main-->Cases;
  Cases-->CaseCreation;
  CaseCreation-->CaseAgreement;
  Cases-->Slides;
  Slides-->DropZone;
  Slides-->UploadQueue;
  UploadQueue-->UploadAgreement;
  Slides-->UploadProgress;
  Slides-->UploadCard;
  Slides-->SlideCard;
  UploadCard-->UploadAgreement;
```

The `DropZone` component uses the [`FilteredFileDrop`](https://gitlab.com/empaia/integration/filtered-file-drop) package to get filtered files from file inputs or file drops. It can be configured for different formats. `Cases` holds an `UploadManager` for each case enabling the upload of files. If a case is selected, `Slides` will show uploading files using the `UploadCard` component. There are additional components (`UploadProgress`, `UploadQueue`) to control the upload and get an overview of the upload progress. Once uploaded `Slides` also holds a list of all `slides`. They are shown using the `SlideCard` component including a thumbnail of the slide which allows for accessing the integrated slide viewer when clicking it. It can also be used to set a tissue and stain type for a slide.

### Install the dependencies

```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)

Follow the quickstart and run

```bash
yarn dev
```

Stop all services and clean volumes by running (no auth)

```bash
docker compose -f docker-compose.yml -f docker-compose-no-auth.yml down -v
```

or (auth)

```bash
docker compose down -v
```

### Lint the files

```bash
yarn run lint
```

### Build the app for production

```bash
quasar build
```

### Customize the configuration

See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
